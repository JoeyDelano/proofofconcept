window.addEventListener('load', init);
function init() {
  googleMaps();
}
function googleMaps() {
  $.ajax({
    url: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAXlNPZ259G7q4KQUE-zmq8wjSsfFd7sT4&libraries=places&callback=initAutocomplete',
    type: 'GET',
    dataType: 'jsonp',
    jsonpcallback: 'callback'
  }).done(initAutocomplete)
}

function locations() {
$.getJSON('./js/locations.json').done(initAutocomplete);
}



function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: 51.448414, lng: 3.561761 },
    zoom: 15,
    mapTypeId: 'roadmap'
  });

  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


  map.addListener('bounds_changed', function () {
    searchBox.setBounds(map.getBounds());
  });
  var markers = [];
  $.getJSON('./js/locations.json').done(function(){
  });
  

  searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    markers.forEach(function (marker) {
      marker.setMap(null);
    });
    markers = [
      ['vlissingen, Terneuzen',51.448414,3.561761]
    ];

    var bounds = new google.maps.LatLngBounds();
    places.forEach(function (place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }

      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));
      console.log(place.geometry.location)

      if (place.geometry.viewport) {
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  var geocoder;
  geocoder = new google.maps.Geocoder();
  
  google.maps.event.addListener(map,'click', function(evt) {
      var lat = evt.latLng.lat();
      var lng = evt.latLng.lng();
      var latlng = new google.maps.LatLng(lat, lng);
      geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
       if (results[1]) {
         console.log(results[1].formatted_address+" "+lat+ " "+lng) ;  //THIS IS YOUR RESULT!!!!
       } else {
         alert('No results found');
       }
      } else {
       alert('Geocoder failed due to: ' + status);
      }
      });
    });
    
}